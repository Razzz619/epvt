﻿// Javascript skeleton.
// Edit and adapt to your needs.
// The documentation of the NeoLoad Javascript API
// is available in the appendix of the documentation.

// Get variable value from VariableManager
var cs1 = context.variableManager.getValue("C_cs1");
var count=cs1.indexOf("KNHS%20Brightspace%20Demo%20Account");
//logger.debug("ComputedValue="+count);
var index=count+5;
var reqString=cs1.substr(index,139);
//logger.debug(reqString);
var csValue=reqString.split("=");
//logger.debug(csValue[1]);
// Inject the computed value in a runtime variable
context.variableManager.setValue("csValue",csValue[1]);
